execute pathogen#infect()
syntax on 
filetype plugin indent on 

"Show status-line always  
set laststatus=2 

"Enable powerline fonts for status line
let g:airline_powerline_fonts=1

"Line numbers 
set relativenumber

"Tabs and spacing 
set tabstop=4 
set shiftwidth=4 
set softtabstop=4
set expandtab 

"For JS and TS
autocmd Filetype javascript setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype typescript setlocal ts=2 sw=2 sts=2 expandtab

"Syntastic
let g:airline#extensions#syntastic#enabled=1
let g:syntastic_cpp_checkers=['gcc']
let g:syntastic_cpp_compiler='g++'

"NERDTree 
let NERDTreeDirArrows=0 

"delimitMate 
let delimitMate_expand_cr=1

"Emmet-vim (<C-Z>>,) 
let g:user_emmet_leader_key='<C-Z>'

"Change tildas color (on blank lines)
highlight EndOfBuffer ctermfg=black

"Plugins used (most of installed with vim-pathogen)
"NERDTree
"Syntastic
"Airline
"delimitMate
"NERD Commenter
"Emmet
